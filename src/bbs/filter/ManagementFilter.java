package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.UserBean;

@WebFilter(urlPatterns = {"/userinfoedit","/userinfo","/signup"})
public class ManagementFilter implements Filter{
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)throws IOException, ServletException {


	  HttpSession session = ((HttpServletRequest)request).getSession();

      if(session == null ){
    	  ((HttpServletResponse)response).sendRedirect("./");
    	  return;
      }
	  UserBean loginCheck = (UserBean) session.getAttribute("loginUser");

	  //権限所持者であるか（本社・総務人事）かどうか確認

	  if(loginCheck.getBranchId() != 1
			  || loginCheck.getDepartmentId() != 1){

		  List<String> messages = new ArrayList<String>();
    	  messages.add("管理者権限がありません。");
    	  session.setAttribute("errorMessages", messages);
    	  ((HttpServletResponse)response).sendRedirect("./");
    	  return;


	  }
	  //filterをかけない
	   chain.doFilter(request, response);
  }

@Override
public void init(FilterConfig paramFilterConfig) throws ServletException {
	// TODO 自動生成されたメソッド・スタブ

}

@Override
public void destroy() {
	// TODO 自動生成されたメソッド・スタブ

}
}
/*
	  //filter処理かけない
	  if(url.equals("/login")){
		  chain.doFilter(request, response);
		  return;
	  }
	  if(url.equals("/css")){
		  chain.doFilter(request, response);
		  return;
	  }

	  //セッションが存在しない
      if(session == null ){
    	  ((HttpServletResponse)response).sendRedirect("/login");

    	  //エラーメッセージ
    	  List<String> messages = new ArrayList<String>();
    	  messages.add("ログイン情報がありません。");
    	  session.setAttribute("errorMessages", messages);
    	  ((HttpServletResponse)response).sendRedirect("./login");
    	  return;
      }
      UserBean loginCheck = (UserBean) session.getAttribute("loginUser");
      if(loginCheck == null){
    	//エラーメッセージ
    	  List<String> messages = new ArrayList<String>();
    	  messages.add("ログイン情報がありません。");
    	  session.setAttribute("errorMessages", messages);
    	  ((HttpServletResponse)response).sendRedirect("./login");
      }
  }

      if (session == null){
         まだ認証されていない
        session = ((HttpServletRequest)request).getSession(true);

        ((HttpServletResponse)response).sendRedirect("/login");
      }else{
        Object loginCheck = session.getAttribute("loginUser");
        if (loginCheck == null){
           まだ認証されていない
          session.setAttribute("target", target);
          ((HttpServletResponse)response).sendRedirect("/login");
        }
      }
  }



@Override
public void init(FilterConfig paramFilterConfig) throws ServletException {
	// TODO 自動生成されたメソッド・スタブ

}

@Override
public void destroy() {
	// TODO 自動生成されたメソッド・スタブ

}
}*/