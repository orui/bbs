package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class UserBean implements Serializable {
	private static long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String name;
	private String password;;
	private int branchId;
	private int departmentId;
	private int contributionId;
	private String isDeleted;
	private Date created_date;
	private Date updated_date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public static void setSerialVersionUID(long serialVersionUID) {
		UserBean.serialVersionUID = serialVersionUID;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}


	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String login_id) {
		this.loginId = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public  void setPassword(String password) {
		this.password = password;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branch_id) {
		this.branchId = branch_id;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int department_id) {
		this.departmentId = department_id;
	}

	public int getContributionId() {
		return contributionId;
	}
	public void setContributionId(int contributionId) {
		this.contributionId = contributionId;
	}
	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String is_deleted) {
		this.isDeleted = is_deleted;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getUpdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

}
