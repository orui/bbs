package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class SearchBean implements Serializable {


	private static long serialVersionUID = 1L;

	private String category;
	private Date createdDate;



	public static void setSerialVersionUID(long serialVersionUID) {
		SearchBean.serialVersionUID = serialVersionUID;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date created_date) {
		this.createdDate = created_date;
	}
}
