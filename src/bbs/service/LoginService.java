package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;

import bbs.beans.UserBean;
import bbs.dao.UserDao;
import bbs.utils.CipherUtil;
public class LoginService {

    public UserBean login(String login, String password, int isDeleted) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            UserBean userBean = userDao.getUser(connection, login, encPassword,isDeleted);

            commit(connection);

            return userBean;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}