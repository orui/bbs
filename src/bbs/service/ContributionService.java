package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.ContributionBean;
import bbs.beans.UserContributionBean;
import bbs.dao.ContributionDao;
import bbs.dao.UserContributionDao;

public class ContributionService {

    public void register(ContributionBean contributionBean) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, contributionBean);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserContributionBean> getContribution(String categorySearch, String beginningDate, String endingDate) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserContributionDao contributionDao = new UserContributionDao();
            List<UserContributionBean> ret = contributionDao.getUserContribution(connection, LIMIT_NUM, categorySearch, beginningDate, endingDate);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void deleteContribution(String contributionId) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.delete(connection, contributionId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



}