package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserBean;
import bbs.dao.UserDao;
import bbs.utils.CipherUtil;

public class UserService {

	public void register(UserBean userBean) {

		Connection connection = null;
		try {
			//接続
			connection = getConnection();
			//パスワードの暗号化
			String encPassword = CipherUtil.encrypt(userBean.getPassword());
			//暗号化されたパスワードをセット
			userBean.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, userBean);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserBean getUser( String loginId, String password,int isDeleted ) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			UserBean user = userDao.getUser(connection,loginId, password, isDeleted);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public UserBean getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			UserBean user = userDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(UserBean userBean) {

		Connection connection = null;
		try {
			connection = getConnection();
			String pass = userBean.getPassword();

			if(!StringUtils.isBlank(pass)){
			String encPassword = CipherUtil.encrypt(pass);
			userBean.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, userBean);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	}