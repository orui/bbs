package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.UserInfoBean;
import bbs.dao.UserDao;
import bbs.dao.UserInfoDao;

public class UserInfoService {

	//アカウント停止復活サービス

	public void update(String id, String isDeleted) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.update(connection, id, isDeleted);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

//    public void register(CommentBean commentBean) {
//
//        Connection connection = null;
//        try {
//            connection = getConnection();
//
//            CommentDao commentDao = new CommentDao();
//            commentDao.insert(connection, commentBean);
//
//            commit(connection);
//        } catch (RuntimeException e) {
//            rollback(connection);
//            throw e;
//        } catch (Error e) {
//            rollback(connection);
//            throw e;
//        } finally {
//            close(connection);
//        }
//    }

    private static final int LIMIT_NUM = 1000;

    public List<UserInfoBean> getUserInfo() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserInfoDao userInfoDao = new UserInfoDao();
            List<UserInfoBean> ret = userInfoDao.getUserInfo(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


	public boolean loginIdCheck(String loginId) {

		Connection connection = null;
		try {
			connection = getConnection();


			return UserInfoDao.loginIdCheck(connection,loginId);


		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean editLoginIdCheck(String loginId, String id) {

		Connection connection = null;
		try {
			connection = getConnection();


			return UserInfoDao.editLoginIdCheck(connection,loginId, id);


		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}