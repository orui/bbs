package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserBean;
import bbs.beans.UserInfoBean;
import bbs.exception.SQLRuntimeException;

public class UserInfoDao {

	public List<UserInfoBean> getUserInfo(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("users.login_id as login_id, ");
//			sql.append("users.branch_id as branch_id, ");
			sql.append("branches.name as branch_name,");
			sql.append("departments.name as department_name,");
//			sql.append("users.department_id as department_id, ");
			sql.append("users.is_deleted as is_deleted, ");
			sql.append("users.created_date as created_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("ORDER BY created_date ASC limit " + num);

			/*
			 *
			 * SELECT 店舗名, s1.名前 店長氏名, s2.名前 副店長氏名
			 * FROM 店舗 t, 社員 s1, 社員 s2
			 * WHERE
			 * t.店長 = s1.社員番号 AND t.副店長 = s2.社員番号
			 *
			 *
			 * select * from テーブルA a,テーブルB b,テーブルC c where (a.CD_A = b.CD_A and
			 * a.CD_B = b.CD_B and a.CD_D = b.CD_D) and (b.CD_A = c.CD_A and
			 * b.CD_B = c.CD_B and b.CD_E = c.CD_E)
			 */

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserInfoBean> ret = toUserInfoList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<UserInfoBean> toUserInfoList(ResultSet rs) throws SQLException {

		List<UserInfoBean> ret = new ArrayList<UserInfoBean>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				String branchName =rs.getString("branch_name");
				String departmentName = rs.getString("department_name");
				int isDeleted = rs.getInt("is_deleted");


				UserInfoBean userInfo = new UserInfoBean();
				userInfo.setLoginId(loginId);
				userInfo.setName(name);
				userInfo.setId(id);
				userInfo.setBranchName(String.valueOf(branchName));
				userInfo.setDepartmentName(String.valueOf(departmentName));
				userInfo.setIsDeleted(isDeleted);

				ret.add(userInfo);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	    public static  boolean loginIdCheck(Connection connection, String loginId){

	    	 PreparedStatement ps = null;
	         try {
	        	 String sql = "SELECT * FROM users WHERE login_id = ?";


	             ps = connection.prepareStatement(sql.toString());

	             ps.setString(1, loginId);

	         	ResultSet rs = ps.executeQuery();

	         	List<UserBean> ret = checkBean(rs);

	         	if(ret.isEmpty() == true){
	         		return true;
	         	}else {
	         		return false;
	         	}



	         } catch (SQLException e) {
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps);
	         }
	    }

	    public static  boolean editLoginIdCheck(Connection connection, String loginId, String id){

	    	 PreparedStatement ps = null;
	         try {
	        	 String sql = "SELECT * FROM users WHERE login_id = ? And id <> ?";


	             ps = connection.prepareStatement(sql.toString());

	             ps.setString(1, loginId);
	             ps.setInt(2, Integer.parseInt(id));

	         	ResultSet rs = ps.executeQuery();

	         	List<UserBean> ret = checkBean(rs);

	         	if(ret.isEmpty() == true){
	         		return true;
	         	}else {
	         		return false;
	         	}

	         } catch (SQLException e) {
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps);
	         }
	    }


	/*    public static  boolean loginIdCheck2(Connection connection, String loginId){

	    	 PreparedStatement ps = null;
	         try {
	        	 String sql = "SELECT * FROM users WHERE login_id = ?";


	             ps = connection.prepareStatement(sql.toString());

	             ps.setString(1, loginId);

	         	ResultSet rs = ps.executeQuery();

	         	List<UserBean> ret = checkBean(rs);

	         	if(ret.isEmpty() == true){
	         		return true;
	         	}else {
	         		return false;
	         	}



	         } catch (SQLException e) {
	             throw new SQLRuntimeException(e);
	         } finally {
	             close(ps);
	         }
	    }

	    */


	    private static List<UserBean> checkBean(ResultSet rs) throws SQLException {

			List<UserBean> ret = new ArrayList<UserBean>();
			try {
				while (rs.next()) {
					String loginId = rs.getString("login_id");

					int id = rs.getInt("id");



					UserBean userInfo = new UserBean();
					userInfo.setLoginId(loginId);
					userInfo.setId(id);


					ret.add(userInfo);
				}
				return ret;
			} finally {
				close(rs);
			}
		}

}