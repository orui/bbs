package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.UserContributionBean;
import bbs.exception.SQLRuntimeException;

public class UserContributionDao {

	public List<UserContributionBean> getUserContribution(Connection connection, int num, String categorySearch, String beginningDate, String endingDate) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("contributions.id as id, ");
			sql.append("contributions.text as text, ");
			sql.append("contributions.user_id as user_id, ");
			sql.append("contributions.title as title, ");
			sql.append("contributions.category as category, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("contributions.created_date as created_date ");
			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");
			sql.append("WHERE contributions.created_date > ? ");
			sql.append("And contributions.created_date < ? ");


			if (categorySearch != null) {
				sql.append("AND category Like ?");
			}
			sql.append("ORDER BY created_date DESC limit " + num);


			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, beginningDate + " 00:00:00");
			ps.setString(2, endingDate +" 23:59:59");

			if (categorySearch != null) {
			ps.setString(3, "%" + categorySearch + "%");
			}

			System.out.println(ps);

			ResultSet rs = ps.executeQuery();
			List<UserContributionBean> ret = toUserContributionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserContributionBean> toUserContributionList(ResultSet rs) throws SQLException {

		List<UserContributionBean> ret = new ArrayList<UserContributionBean>();
		try {
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserContributionBean contribution = new UserContributionBean();
				contribution.setLoginId(loginId);
				contribution.setName(name);
				contribution.setId(id);
				contribution.setUserId(userId);
				contribution.setTitle(title);
				contribution.setText(text);
				contribution.setCategory(category);
				contribution.setCreated_date(createdDate);

				ret.add(contribution);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}