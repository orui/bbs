package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.CommentBean;
import bbs.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<CommentBean> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //DBから表結合して抽出するSQL文
            //usersとcomments
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.contribution_id as contribution_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<CommentBean> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<CommentBean> toCommentList(ResultSet rs)
            throws SQLException {

        List<CommentBean> ret = new ArrayList<CommentBean>();
        try {
            while (rs.next()) {
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                int contributionId = rs.getInt("contribution_Id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                CommentBean comment = new CommentBean();
                comment.setLoginId(loginId);
                comment.setName(name);
                comment.setId(id);
                comment.setUserId(userId);
                comment.setContributionId(contributionId);
                comment.setText(text);
                comment.setCreatedDate(createdDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}