package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import bbs.beans.ContributionBean;
import bbs.exception.SQLRuntimeException;

public class ContributionDao {

    public void insert(Connection connection, ContributionBean contributionBean) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO contributions ( ");
            sql.append("user_id");
            sql.append(", title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?");//title
            sql.append(", ?");// text
            sql.append(", ?");//category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, contributionBean.getUserId());
            ps.setString(2, contributionBean.getTitle());
            ps.setString(3, contributionBean.getText());
            ps.setString(4, contributionBean.getCategory());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //投稿削除
    public void delete(Connection connection, String contributionId){

    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("DELETE FROM ");
             sql.append("contributions ");
             sql.append("WHERE id = ? ");


             ps = connection.prepareStatement(sql.toString());

            	 ps.setInt(1, Integer.parseInt(contributionId));


             ps.executeUpdate();
         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }

}


//	public ResultSet getSearch(Connection connection, int num) {
//
//		PreparedStatement ps = null;
//		try {
//			StringBuilder sql = new StringBuilder();
//			sql.append("SELECT");
//			sql.append(" category");
//			//sql.append(", created_date");
//			sql.append(" FROM");
//			sql.append("contributions");
//			sql.append(" WHERE");
//			sql.append(" category = ?");
//			//sql.append("AND created_date = ?");
//			sql.append(")");
//
//			ps = connection.prepareStatement(sql.toString());
//
//			ps.setString(1, "category");
//			//ps.set(2, SearchBean.getCreatedDate());
//
//			System.out.println(ps.toString());
//			// sql文の実行
//			ResultSet rs = ps.executeQuery();
//			return rs;
//		} catch (SQLException e) {
//			throw new SQLRuntimeException(e);
//		} finally {
//			close(ps);
//		}
//
//}


