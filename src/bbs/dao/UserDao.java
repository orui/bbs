package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
//追記用
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserBean;
import bbs.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, UserBean userBean) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("name");
			sql.append(", login_id");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_deleted");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // name
			sql.append(", ?"); // loginId
			sql.append(", ?"); // passward
			sql.append(", ?"); // branch
			sql.append(", ?"); // department
			sql.append(", 0"); // isDeleated
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, userBean.getName());
			ps.setString(2, userBean.getLoginId());
			ps.setString(3, userBean.getPassword());
			ps.setInt(4, userBean.getBranchId());
			ps.setInt(5, userBean.getDepartmentId());
			// ps.setString(6, user.getIsDeleted());
			System.out.println(ps.toString());
			// sql文の実行
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public UserBean getUser(Connection connection, String login_id, String password, int is_deleted) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);
			ps.setInt(3, is_deleted);

			System.out.println(ps);

			ResultSet rs = ps.executeQuery();
			List<UserBean> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBean> toUserList(ResultSet rs) throws SQLException {

		List<UserBean> ret = new ArrayList<UserBean>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginId = rs.getString("login_id");
				String branch = rs.getString("branch_id");
				String department = rs.getString("department_id");
				String password = rs.getString("password");
				String Deleted = rs.getString("is_deleted");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				UserBean userBean = new UserBean();
				userBean.setId(id);
				userBean.setLoginId(loginId);
				userBean.setName(name);
				userBean.setBranchId(Integer.parseInt(branch));
				userBean.setPassword(password);
				userBean.setDepartmentId(Integer.parseInt(department));
				userBean.setIsDeleted(Deleted);
				userBean.setCreatedDate(createdDate);
				userBean.setUpdatedDate(updatedDate);

				ret.add(userBean);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public UserBean getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<UserBean> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, UserBean userBean) {

		PreparedStatement ps = null;
		String pass = userBean.getPassword();
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("  name = ?");
			sql.append(", login_id = ?");
			//passwordが入力されていない場合に更新されるように

			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			if(!StringUtils.isBlank(pass)){
			sql.append(", password = ?");
			}
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, userBean.getName());
			ps.setString(2, userBean.getLoginId());
			ps.setInt(3, userBean.getBranchId());
			ps.setInt(4, userBean.getDepartmentId());

			if(!StringUtils.isBlank(pass)){
			ps.setString(5, pass);
			ps.setInt(6, userBean.getId());
			}else{

			ps.setInt(5, userBean.getId());
			}


			System.out.println(ps.toString());
			// sql文の実行
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public void update(Connection connection, String id, String isDeleted) {

		//アカウントの停止復活
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("  is_deleted = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, Integer.parseInt(isDeleted));
			ps.setInt(2, Integer.parseInt(id));


			System.out.println(ps.toString());
			// sql文の実行
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
