package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

@Override
protected void doPost(HttpServletRequest request,
HttpServletResponse response) throws IOException, ServletException {

	String commentId = request.getParameter("commentId");

	new CommentService().deleteComment(commentId);

	response.sendRedirect("./");

	}
}