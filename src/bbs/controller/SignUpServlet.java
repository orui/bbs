package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserBean;
import bbs.service.UserInfoService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();
        //セッションをリクエスト
        HttpSession session = request.getSession();
        //有効値の確認
        if (isValid(request, messages) == true) {

        	//JSPで入力された値をリクエスト（取得）
        	//beans.userのName等にセット
            UserBean userBean = new UserBean();
            userBean.setName(request.getParameter("name"));
            userBean.setLoginId(request.getParameter("loginId"));
            userBean.setBranchId(Integer.parseInt(request.getParameter("branch")));
            userBean.setDepartmentId(Integer.parseInt(request.getParameter("department")));
            userBean.setPassword(request.getParameter("password"));

            new UserService().register(userBean);

            response.sendRedirect("userinfo");
        } else {
            session.setAttribute("errorMessages", messages);

            UserBean userBean = new UserBean();
            userBean.setName(request.getParameter("name"));
            userBean.setLoginId(request.getParameter("loginId"));
            userBean.setBranchId(Integer.parseInt(request.getParameter("branch")));
            userBean.setDepartmentId(Integer.parseInt(request.getParameter("department")));

            request.setAttribute("userBean", userBean);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String name = request.getParameter("name");
    	String loginId = request.getParameter("loginId");
    	int branch = Integer.parseInt(request.getParameter("branch"));
    	int department = Integer.parseInt(request.getParameter("department"));
        String password = request.getParameter("password");
        String passwordCheck = request.getParameter("passwordCheck");

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isBlank(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }

        //// 記号を含む全ての半角文字で6文字以上20文字以下の確認
        else if (!loginId.matches("[!-~]{6,20}")) {
   			messages.add("ログインIDを6字以上20字以下で入力してください");
   		} else if (new UserInfoService().loginIdCheck(loginId)==false){
   			messages.add("ログインIDが重複しています");
   		}


/*        if (StringUtils.isBlank(branchId) == true) {
            messages.add("支店を選択してください");
        }
        if (StringUtils.isBlank(departmentId) == true) {
            messages.add("部署または役職を選択してください");
        }*/
        if (StringUtils.isBlank(password) == true) {
            messages.add("パスワードを入力してください");
        }else if(!password.matches("[ -~]{6,20}")){
        	messages.add("パスワードを6字以上20字以下で入力してください");
        }

        if (!password.equals(passwordCheck)){
        	messages.add("確認用パスワードに誤りがあります");
        }
        if (branch == 0){
        	messages.add("支店を選択してください");
        }
        if (department == 0){
        	messages.add("部署・役職を選択してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}