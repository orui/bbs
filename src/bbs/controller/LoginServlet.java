package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.UserBean;
import bbs.service.LoginService;

@WebServlet(urlPatterns =  { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

//        String loginId = request.getParameter("loginId");
//    	request.setAttribute("loginUser", loginId);

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();

        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        int isDeleted = 0;
		UserBean userBean = loginService.login(loginId,password,isDeleted);


        if (userBean != null&& isDeleted == 0) {
        	//userBeanをloginUserに入れる
            session.setAttribute("loginUser", userBean);
            response.sendRedirect("./");
        } else {
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました");
            session.setAttribute("errorMessages", messages);

            //JSP側で入力されたパラメーターを送り返す
            request.setAttribute("loginUser", request.getParameter("loginId"));
    		request.getRequestDispatcher("login.jsp").forward(request, response);

        }
    }

}
