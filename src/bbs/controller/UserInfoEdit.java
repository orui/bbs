package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.UserBean;
import bbs.service.UserInfoService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/userinfoedit" })
public class UserInfoEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		// セッションよりログインユーザーの情報を取得
		String userId = request.getParameter("id");
		//UserBean loginUser = (UserBean) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();
		try{


		// ログインユーザー情報のidを元にDBからユーザー情報取得
		UserBean editUser = new UserService().getUser(Integer.parseInt(userId));

		//ここで支店部署情報をjspへ送る



		if(editUser == null){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userinfo");
			return;
		}
		if(StringUtils.isBlank(userId)){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userinfo");
			return;
		}{
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("userinfoedit.jsp").forward(request, response);

		}

		}
		catch(NumberFormatException e){
			messages.add("不正なパラメーターです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userinfo");
			return;
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		UserBean editUser = getEditUser(request);


		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (bbs.exception.NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("userinfoedit.jsp").forward(request, response);
				return;
			}

			session.setAttribute("editUser", editUser);
			response.sendRedirect("userinfo");

		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("userinfoedit.jsp").forward(request, response);
		}
	}

	private UserBean getEditUser(HttpServletRequest request) throws IOException, ServletException {

		UserBean editUser = new UserBean();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartmentId(Integer.parseInt(request.getParameter("department")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String branchId = request.getParameter("branch");
		String departmentId = request.getParameter("department");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");


		if (id == null){
			messages.add("不正なパラメーターです");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		// ログインIDの空欄の確認
		if (StringUtils.isBlank(loginId)) {
			messages.add("ログインIDを入力してください");
		}
        //// 記号を含む全ての半角文字で6文字以上20文字以下の確認
        else if (!loginId.matches("[!-~]{6,20}")) {
   			messages.add("ログインIDを6字以上20字以下で入力してください");
   		}
        if (new UserInfoService().editLoginIdCheck(loginId,id)==false){
   			messages.add("ログインIDが重複しています");
   		}

		if (StringUtils.isEmpty(departmentId) == true) {
			messages.add("部署または役職を選択してください");
		}
		// パスワード値が空欄ではないかの確認
		if (!StringUtils.isBlank(password)) {

			// 記号を含む全ての半角文字で6文字以上20文字以下の確認
			if (!password.matches("[ -~]{6,20}")) {
				messages.add("パスワードを6字以上20字以下で入力してください");
			}
			// 確認用パスワードとの一致確認
			if (!password.equals(passwordCheck)) {
				messages.add("確認用パスワードに誤りがあります");
			}
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}