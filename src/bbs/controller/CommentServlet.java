package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.CommentBean;
import bbs.beans.UserBean;
import bbs.service.CommentService;

//当該サーブレット機能のURL指定
@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	//jsp画面の立ち上げ
        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();


        UserBean userBean = (UserBean) session.getAttribute("loginUser");

        CommentBean comment = new CommentBean();
        comment.setText(request.getParameter("comment"));
        comment.setName(userBean.getName());
        comment.setUserId(userBean.getId());
        comment.setCreatedDate(userBean.getCreatedDate());
        comment.setContributionId(userBean.getContributionId());

        if (isValid(request, comments) == true) {


            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", comments);

			request.setAttribute("comment", comment);
			request.getRequestDispatcher("./").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            comments.add("メッセージを入力してください");
        }
        if (500 < comment.length()) {
            comments.add("1000文字以下で入力してください");
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}