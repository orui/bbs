package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.CommentBean;
import bbs.beans.UserBean;
import bbs.beans.UserContributionBean;
import bbs.service.CommentService;
import bbs.service.ContributionService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {


		// 検索条件をjspより取得
		String categorySearch = request.getParameter("categorySearch");
		String beginningDate = request.getParameter("beginningDate");
		String endingDate = request.getParameter("endingDate");


		if (categorySearch==""||categorySearch==null) {
			categorySearch = "";
		}
		/*} else if (10 < categorySearch.length()) {
			messages.add("カテゴリーを10文字以下で入力してください");
			session.setAttribute("errorMessages", messages);*/

		Calendar calendar = Calendar.getInstance();
		String currentCalendar = calendar.getTime().toString();
		System.out.println(currentCalendar);

		if (beginningDate==""||beginningDate==null) {
			beginningDate = "2018/05/01";
		}
		if (endingDate==""||endingDate==null) {
			endingDate = currentCalendar;
		}



		List<UserContributionBean> contributions = new ContributionService().getContribution(categorySearch,
				beginningDate, endingDate);
		List<CommentBean> comments = new CommentService().getComment();

		request.setAttribute("contributions", contributions);
		request.setAttribute("comments", comments);
		/*
		 * request.setAttribute("categorySearch", categorySearch);
		 * request.setAttribute("beginningDate", beginningDate);
		 * request.setAttribute("endingDate", endingDate);
		 */
		// 検索結果をjspへ返す

		//検索キーワード値の保持
        request.setAttribute("categorySearch",categorySearch);
        request.setAttribute("beginningDate",beginningDate);
        request.setAttribute("endingDate",endingDate);

		request.getRequestDispatcher("/top.jsp").forward(request, response);

	}



	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comments = new ArrayList<String>();


		UserBean userBean = (UserBean) session.getAttribute("loginUser");

		CommentBean comment = new CommentBean();
		comment.setText(request.getParameter("comment"));

		comment.setContributionId(Integer.parseInt(request.getParameter("contributionId")));
		comment.setName(userBean.getName());
		comment.setUserId(userBean.getId());
		comment.setCreatedDate(userBean.getCreatedDate());


		if (isValid(request, comments) == true) {
;

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", comments);
			session.setAttribute("comment",comment);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}