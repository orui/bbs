package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.beans.UserInfoBean;
import bbs.service.UserInfoService;


@WebServlet(urlPatterns = { "/userinfo" })
public class UserInfoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	//jspへ値を表示させる
        List<UserInfoBean> userInfo = new UserInfoService().getUserInfo();

        request.setAttribute("userInfo", userInfo);

        request.getRequestDispatcher("/userinfo.jsp").forward(request, response);
    }




    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {



    	   String id = request.getParameter("id");
    	   String isDeleted = request.getParameter("isDeleted");

    	   new UserInfoService().update(id, isDeleted);

    	   response.sendRedirect("userinfo");



       /*    List<String> comments = new ArrayList<String>();


            UserBean userBean = (UserBean) session.getAttribute("loginUser");

            CommentBean comment = new CommentBean();
            comment.setText(request.getParameter("comment"));
            comment.setName(userBean.getName());
            comment.setUserId(userBean.getId());
            comment.setCreatedDate(userBean.getCreatedDate());
            comment.setContributionId(userBean.getContributionId());*/



    }
}