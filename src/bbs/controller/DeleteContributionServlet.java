package bbs.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bbs.service.ContributionService;

@WebServlet(urlPatterns = { "/deleteContribution" })
public class DeleteContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

@Override
protected void doPost(HttpServletRequest request,
HttpServletResponse response) throws IOException, ServletException {

	String contributionId = request.getParameter("contributionId");
//	String  deleteContribution = request.getParameter("deleteContribution");

	new ContributionService().deleteContribution(contributionId);

	response.sendRedirect("./");

	}
}