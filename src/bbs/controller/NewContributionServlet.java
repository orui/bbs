package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.ContributionBean;
import bbs.beans.UserBean;
//import bbs.beans.User;
import bbs.service.ContributionService;

//当該サーブレット機能のURL指定
@WebServlet(urlPatterns = { "/contribution" })
public class NewContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// jsp画面の立ち上げ
		request.getRequestDispatcher("contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		UserBean userBean = (UserBean) session.getAttribute("loginUser");

		ContributionBean contributionBean = new ContributionBean();
		contributionBean.setText(request.getParameter("message"));
		contributionBean.setTitle(request.getParameter("title"));
		contributionBean.setCategory(request.getParameter("category"));
		contributionBean.setName(userBean.getName());
		contributionBean.setUserId(userBean.getId());

		if (isValid(request, messages) == true) {

			new ContributionService().register(contributionBean);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			request.setAttribute("contributionBean", contributionBean);
			request.getRequestDispatcher("contribution.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String message = request.getParameter("message");

		if (StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}
		if (30 < title.length()) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (StringUtils.isBlank(message) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < message.length()) {
			messages.add("1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}