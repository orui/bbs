<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
     <link href="./css/style.css" rel="stylesheet" type="text/css"></link>
     <link href="./css/comment.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <div align="center">
   <h1>ユーザー新規登録</h1>
   </div>
     <div align="right"><a href="logout">ログアウト</a></div>

        <div class="main-contents">
        <div class="signup2">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            ・<c:out value="${message}" /><br>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            </div>

            <form action="signup" method="post" class="center signup1">
                <br /> <label for="name">名前</label> <input name="name" id="name" value="${userBean.name}"/><br />
                <label for="loginId">ログインID</label> <input name="loginId"
                    id="loginId" value="${userBean.loginId}"/> <br /> <label for="password">パスワード</label> <input
                    name="password" type="password" id="password" /> <br />
                    <label for="passwordCheck">確認用パスワード</label> <input name="passwordCheck"
                    type="password" id="passwordCheck" /><br />



                    <label for="branch">支店</label>
                    <select name="branch"  value="${userBean.branchId}">
                    <option value="0">選択してください</option>
                    <option value="1">本社</option>
                    <option value="2">支店A</option>
                    <option value="3">支店B</option>
                    <option value="4">支店C</option>
                    </select><br />

                    <label for="department">部署または役職</label>
                    <select name="department" value="${userBean.departmentId}">
                    <option value="0">選択してください</option>
                    <option value="1">総務人事</option>
                    <option value="2">情報管理</option>
                    <option value="3">支店長</option>
                    <option value="4">社員</option>
                    </select><br />

                <br /> <input type="submit" value="登録" /> <br /> <br />
                <a href="userinfo">戻る</a>
            </form>
            <div align="center" class="copyright"> Copyright(c)SatsukiOrui</div>
        </div>
    </body>
</html>