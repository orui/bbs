<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BBS</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/comment.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div align="center">
		<h1>BBS HOME</h1>
	</div>



	<div align="right">
		<a href="logout">ログアウト</a>
	</div>

	<a href="contribution">新規投稿</a>

	<!-- 管理権限のあるユーザーのみリンクを表示 -->
	<c:if test="${loginUser.branchId == 1 && loginUser.departmentId == 1}">
		<a href="userinfo">ユーザー管理</a>
	</c:if>



	<br />
	<br />

	<form action="./" method="get">
		<p>＜検索機能＞</p>
		<input type="text" name="categorySearch" placeholder="カテゴリーを入力"
			value="${categorySearch}"> <br /> <br /> <input type="date"
			name="beginningDate" placeholder="開始日を入力" value="${beginningDate}">
		～ <input type="date" name="endingDate" placeholder="終了日を入力"
			value="${endingDate}"> <input type="submit" value="検索">
		<input type="button" onclick="location.href='./'" value="リセット">
	</form>

	<!-- 投稿の表示 -->
	<div class="contribution">
		<c:forEach items="${contributions}" var="contributionBean">
			<div class="box12">
				<div class="contribution">
					<div class="title">
						<span class="title"><c:out
								value="${contributionBean.title}" /></span><br>
					</div>

					<hr style="border: 2px solid #87CEEB;">

					<div class="title">
						<span class="category"><span class="category2">カテゴリー
								: </span> <c:out value="${contributionBean.category}" /> </span>
					</div>
					<br>

					<!-- 改行が含んだ文 -->
					<div class="textbreak">
						<c:forEach var="newLine"
							items="${fn:split(contributionBean.text,'
')}">
							<c:out value="${newLine}" />
							<br />
						</c:forEach>
					</div>
				</div>



				<br> <span class="name">投稿者：<c:out
						value="${contributionBean.name}" /></span>

				<div class="date">
					<fmt:formatDate value="${contributionBean.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>


				<c:if test="${contributionBean.userId == loginUser.id}">
					<form action="deleteContribution" method="post">
						<input type="hidden" name="contributionId"
							value="${contributionBean.id}"> <input type="submit"
							value="削除">
					</form>
				</c:if>


			</div>

			<!-- コメントの表示 -->

			<c:forEach items="${comments}" var="commentBean">

				<c:if test="${contributionBean.id == commentBean.contributionId}">
					<div class="box23">
						<div class="comment">


							<!-- 改行が含んだ文 -->
							<c:forEach var="newLine"
								items="${fn:split(commentBean.text,'
')}">
								<c:out value="${newLine}" />
								<br />
							</c:forEach>
							<br> <span class="name">コメント者：<c:out
									value="${commentBean.name}" /></span><br>
							<div class="date">
								<fmt:formatDate value="${commentBean.createdDate}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</div>
						</div>


						<c:if test="${commentBean.userId == loginUser.id}">
							<!--5/18 訂正 -->
							<form action="deleteComment" method="post">
								<input type="hidden" name="commentId" value="${commentBean.id}">
								<input type="submit" value="削除">
							</form>
						</c:if>
					</div>
				</c:if>

			</c:forEach>


			<%-- コメント投稿 --%>
			<div class="box23">
				<form action="./" method="post">

					<textarea name="comment" cols="100" rows="3" class="tweet-box"><c:if test="${contributionBean.id == comment.contributionId}">${comment.text}</c:if></textarea>


						<!-- エラーメッセージの表示 -->
					<c:if test="${contributionBean.id == comment.contributionId}">
						<c:if test="${ not empty errorMessages }">
							<div class="errorMessages">
								<ul>
									<c:forEach items="${errorMessages}" var="message">
										・<c:out value="${message}" />
									</c:forEach>
								</ul>
							</div>
							<c:remove var="errorMessages" scope="session" />
						</c:if>
					</c:if>

					<br /> （500文字まで） <input type="hidden" name="contributionId"
						value="${contributionBean.id}"> <input type="submit"
						value="コメントする"> <br> <br>

				</form>
			</div>
			<br>
		</c:forEach>
	</div>
	<br>
	<a href="./">ページトップへ</a>
	<div align="center">
		<div class="copyright">Copyright(c)SatsukiOrui</div>
	</div>
	<c:remove var="comment" scope="session" />
</body>
</html>