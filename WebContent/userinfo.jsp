<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>BBS</title>
<link href="./css/style.css" rel="stylesheet" type="text/css"></link>
<link href="./css/comment.css" rel="stylesheet" type="text/css"></link>

<!-- jQuery -->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<!--アカウントの停止-->
<!-- ダイアログ設定 -->
<script>
	function stopChk() {
		/* 確認ダイアログ表示 */
		var flag = confirm("このユーザーを停止してもよろしいですか？\n\n停止させたくない場合は[キャンセル]ボタンを押して下さい");
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
</script>

<!--アカウントの復活 -->
<!-- ダイアログ設定 -->
<script>
	function restartChk() {
		/* 確認ダイアログ表示 */
		var flag = confirm("このユーザーを復活してもよろしいですか？\n\n復活させたくない場合は[キャンセル]ボタンを押して下さい");
		/* send_flg が TRUEなら送信、FALSEなら送信しない */
		return flag;
	}
</script>






</head>
<body>
	<div align="center">
		<h1>ユーザー管理</h1>
	</div>

	<div align="right">
		<a href="logout">ログアウト</a>
	</div>

<div class="main-contents">
		<div class="signup2">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							・<c:out value="${message}" /><br>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
	</div>


	<a href="./">BBS HOME</a>
	<br>
	<a href="signup">ユーザー新規登録</a>
	<br>
		<table border="1" class="center2">
			<tr>
				<th>名前</th>
				<th>ログインID</th>
				<th>支店名</th>
				<th>部署・役職</th>
				<th>ステータス変更</th>
				<th>ユーザー編集</th>
			</tr>

			<!-- <div class="userinfo"> -->

			<c:forEach items="${userInfo}" var="userInfoBean">

				<input name="id" value="${userBean.id}" id="id" type="hidden" />
				<tr>
					<td><span class="name"><c:out
								value="${userInfoBean.name}" /></span></td>

					<td><span class="loginId"><c:out
								value="${userInfoBean.loginId}" /></span></td>

					<td><span class="branch"><c:out
								value="${userInfoBean.branchName}" /></span></td>

					<td><span class="department"><c:out
								value="${userInfoBean.departmentName}" /></span></td>

					<%-- <span class="isDeleted"><c:out value="${userInfoBean.isDeleted}" /></span> --%>

					<td><c:if test="${userInfoBean.id != loginUser.id}">

							<c:if test="${userInfoBean.isDeleted == 0}">
								<form action="userinfo" method="post"
									onsubmit="return stopChk()">
									<input type="submit" value="停止" class="button2">
									<input type="hidden"
										name="id" value="${userInfoBean.id}"> <input
										type="hidden" name="isDeleted" value="1">

								</form>
							</c:if>

							<c:if test="${userInfoBean.isDeleted == 1}">
								<form action="userinfo" method="post"
									onsubmit="return restartChk()">
									<!-- <div class="restart"> -->
									<input type="submit" value="復活" class="button">
									</div>
									<input type="hidden" name="id" value="${userInfoBean.id}">
									<input type="hidden" name="isDeleted" value="0">

								</form>

							</c:if>
						</c:if> <c:if test="${userInfoBean.id == loginUser.id}">
							<input type="button" disabled value="ログイン中" class="button3">
						</c:if></td>
					<td><a href="userinfoedit?id=${userInfoBean.id}">編集</a></td>
			</c:forEach>
			<tr>
		</table>
	<br>
	<a href="./">BBS HOMEへ戻る</a>

	<div align="center">
		<div class="copyright">Copyright(c)SatsukiOrui</div>
	</div>

</body>
</html>