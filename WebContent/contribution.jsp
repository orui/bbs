<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BBS</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/comment.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div align="center">
		<h1>新規投稿</h1>
	</div>

	<!-- エラーメッセージの表示 -->

	<div class="main-contents">
		<div class="signup2">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							・<c:out value="${message}" /><br>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
	</div>

	<!-- <form action = POSTする送信先（サーブレット）method･･･ -->

		<form action="contribution" method="post" class="center signup4">

			件名<br /> <input type="text" name="title" size="30" maxlength="50" value="${contributionBean.title}">（30文字まで）
			<br />
			カテゴリー<br /> <input type="text" name="category" size="30" maxlength="30" value="${contributionBean.category}">（10文字まで）
			<br />

			本文（1000文字まで）<br />
			<textarea name="message" cols="50" rows="5" class="post-box">${contributionBean.text}</textarea>
			<br>
			<br><input type="submit" value="送信">

		<br />
		<br />
		<a href="./">戻る</a>
		</form>

	<br>

	<br />
	<div class="copyright">Copyright(c)SatsukiOrui</div>
</body>
</html>