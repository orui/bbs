<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー情報編集</title>
<link href="./css/style.css" rel="stylesheet" type="text/css"></link>
<link href="./css/comment.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div align="center">
		<h1>ユーザー情報編集</h1>
	</div>
	<div align="right">
		<a href="logout">ログアウト</a>
	</div>
	<div class="main-contents">
		<div class="signup2">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							・<c:out value="${message}" /><br>
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
	</div>


		<form action="userinfoedit" method="post" class="center signup1">
			<br />
			<!-- サーブレットで受け渡されたrequest.setAttribute("editUser", editUser);
                をJSP側value で表記させる ＝ 値の保持-->
			<input type="hidden" name="id" value="${editUser.id}"> <label
				for="name">名前</label> <input name="name" id="name"
				value="${editUser.name}" /><br /> <label for="loginId">ログインID</label>
			<input name="loginId" value="${editUser.loginId}" id="loginId" /> <br />
			<label for="password">パスワード</label> <input name="password"
				type="password" id="password" /> <br /> <label for="passwordCheck">確認用パスワード</label>
			<input name="passwordCheck" type="password" id="passwordCheck" /><br />



			<c:if test="${loginUser.id != editUser.id}">

				<label for="branch">支店</label>
				<select name="branch">

					<c:if test="${editUser.branchId == 1}">
						<option value="1" selected>本社</option>
					</c:if>
					<c:if test="${editUser.branchId != 1}">
						<option value="1">本社</option>
					</c:if>
					<c:if test="${editUser.branchId == 2}">
						<option value="2" selected>支店A</option>
					</c:if>
					<c:if test="${editUser.branchId != 2}">
						<option value="2">支店A</option>
					</c:if>
					<c:if test="${editUser.branchId == 3}">
						<option value="3" selected>支店B</option>
					</c:if>
					<c:if test="${editUser.branchId != 3}">
						<option value="3">支店B</option>
					</c:if>
					<c:if test="${editUser.branchId == 4}">
						<option value="4" selected>支店C</option>
					</c:if>
					<c:if test="${editUser.branchId != 4}">
						<option value="4">支店C</option>
					</c:if>


				</select>
				<br />


				<label for="department">部署または役職</label>
				<select name="department">

					<c:if test="${editUser.departmentId == 1}">
						<option value="1" selected>総務人事</option>
					</c:if>
					<c:if test="${editUser.departmentId != 1}">
						<option value="1">総務人事</option>
					</c:if>
					<c:if test="${editUser.departmentId == 2}">
						<option value="2" selected>情報管理</option>
					</c:if>
					<c:if test="${editUser.departmentId != 2}">
						<option value="2">情報管理</option>
					</c:if>
					<c:if test="${editUser.departmentId == 3}">
						<option value="3" selected>支店長</option>
					</c:if>
					<c:if test="${editUser.departmentId != 3}">
						<option value="3">支店長</option>
					</c:if>
					<c:if test="${editUser.departmentId == 4}">
						<option value="4" selected>社員</option>
					</c:if>
					<c:if test="${editUser.departmentId != 4}">
						<option value="4">社員</option>
					</c:if>

				</select>
				<br />



			</c:if>


	<br />
	<input type="submit" value="変更" />
	<br />
	<br />
	<a href="userinfo">戻る</a>

	</form>

	<div align="center" class="copyright">Copyright(c)SatsukiOrui</div>
	<!--   </div> -->
</body>
</html>